import { createStore } from 'redux';
import reducer, { initialState
} from '../../app/features/weather/reducers';
import { fetchWeatherSuccess } from '../../app/features/weather/actions';

const weatherApiJson = {
    "temperature":  6.87,
    "unit": "Celsius",
    "where": "London",
    "description": "Light rain",
    "service-name": "Open Weather"
};

describe('reducer', () => {
  let store;
  beforeEach(() => {
    store = createStore(reducer.openWeather);
  });

  it('should use initial state if state not provided', () => {
    const state = store.getState();
    expect(state).toEqual(initialState);
  });

  it('should return update weather data on fetch success', () => {
    store.dispatch(fetchWeatherSuccess(weatherApiJson));
    const state = store.getState();
    expect(state.data).toEqual(weatherApiJson);
    expect(state.loaded).toBe(true);
    expect(state.fetching).toBe(false);
  });

});
