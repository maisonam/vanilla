import { createStore } from 'redux';
import reducer, { initialState
} from '../../app/features/core/reducers/app';
import { toggleMenu } from '../../app/features/core/actions';

describe('reducer', () => {
  let store;
  beforeEach(() => {
    store = createStore(reducer.app);
  });

  it('should use initial state if state not provided', () => {
    const state = store.getState();
    expect(state).toEqual(initialState);
  });

  it('should toggle menu status on TOGGLE', () => {
    store.dispatch(toggleMenu());
    const state = store.getState();
    const menuStatus = store.getState().menuOpen;
    expect(initialState.menuOpen).toEqual(!menuStatus);
  });

});
