import { createStore } from 'redux';
import reducer, { initialState
} from '../../app/features/tremendousweather/reducers';
import { fetchTremendousWeatherSuccess } from '../../app/features/tremendousweather/actions';

const weatherApiJson = {
    "coord": {
        "lon": -0.13,
        "lat": 51.51
    },
    "weather": [
        {
            "id": 300,
            "main": "Drizzle",
            "description": "light intensity drizzle",
            "icon": "09d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": {
            "unit": "kelvin",
            "value": 280.32
        },
        "pressure": 1012,
        "humidity": 81,
        "temp_min": 279.15,
        "temp_max": 281.15
    },
    "visibility": 10000,
    "wind": {
        "speed": 4.1,
        "deg": 80
    },
    "clouds": {
        "all": 90
    },
    "dt": 1485789600,
    "sys": {
        "type": 1,
        "id": 5091,
        "message": 0.0103,
        "country": "GB",
        "sunrise": 1485762037,
        "sunset": 1485794875
    },
    "id": 2643743,
    "name": "London",
    "cod": 200,
    "service": {
        "name": "Tremendous weather"
    }
};


describe('reducer', () => {
  let store;
  beforeEach(() => {
    store = createStore(reducer.tremendousWeather);
  });

  it('should use initial state if state not provided', () => {
    const state = store.getState();
    expect(state).toEqual(initialState);
  });

  it('should return update weather data on fetch success', () => {
    store.dispatch(fetchTremendousWeatherSuccess(weatherApiJson));
    const state = store.getState();
    expect(state.data).toEqual(weatherApiJson);
    expect(state.loaded).toBe(true);
    expect(state.fetching).toBe(false);
  });

});
