import React from 'react';
import { shallow } from 'enzyme';
import { Weather } from '../../app/features/weather';

const weather = {
  data: {
  "temperature": 6.87,
  "unit": "Celsius",
  "where": "London",
  "description": "Light rain",
  "service-name": "Open Weather"
  }
};

const defaultProps = {
  weather,
  onfetchWeather: (f) => f,
};

describe('ItemCreator', () => {
  it('renders without crashing', () => {
    shallow(<Weather {...defaultProps} />);
  });
  
});
