import React from 'react';
import { shallow } from 'enzyme';
import { Menu } from '../../app/features/core/components/Menu';

const defaultProps = {
  open: false,
  toggle: (f) => f,
};

describe('Menu', () => {
  it('renders without crashing', () => {
    shallow(<Menu {...defaultProps} />);
  });
  it('should call toggle function on hamburger button CLICK', () => {
    const onToggleMock = jest.fn();
    const renderedMenu = shallow(<Menu {...defaultProps} toggle={onToggleMock} />);
    renderedMenu.find('#menu-btn').simulate('click');
    expect(onToggleMock.mock.calls.length).toBe(1);
  });
});
