const fs = require('fs');
const path = require('path');

const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = relativePath => path.resolve(appDirectory, relativePath);


module.exports = {
  app: resolvePath('app'),
  appHtml: resolvePath('assets/index.html'),
  appIndexJs: resolvePath('app/index.jsx'),
  appScripts: resolvePath('app/scripts'),
  coreComponents: resolvePath('app/features/core/components/'),
  coreReducers: resolvePath('app/features/core/reducers/'),
  coreEpics: resolvePath('app/features/core/epics/'),
  assets: resolvePath('assets'),
  config: resolvePath('config'),
  dist: resolvePath('dist'),
  dotenv: resolvePath('.env'),
  modernizr: resolvePath('app/scripts/vendor/modernizr-custom.js'),
  modernizrrc: resolvePath('config/modernizrrc.json'),
  nodeModules: resolvePath('node_modules'),
  packageJson: resolvePath('package.json'),
  publicPath: resolvePath('/'),
  root: resolvePath(''),
  store: resolvePath('app/store/index'),
  test: resolvePath('test'),
};
