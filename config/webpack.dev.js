const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');
const webpackConfig = require('./webpack.config.js');

// the clean options to use
let cleanOptions = {
  root:     paths.app,
  verbose:  true,
  dry:      false,
  allowExternal: true
}

module.exports = merge(webpackConfig, {
  mode: 'development',
  entry: {
    main: [
      'babel-polyfill',
      'event-source-polyfill',
      'react-hot-loader/patch',
      paths.appIndexJs
    ]
  },
  module: {
   rules: [
     {
       test: /\.js$/,
       enforce: 'pre',
       exclude: /node_modules/,
       use: ['eslint-loader']
     },
   ]
  },
  output: {
    path: paths.dist,
    publicPath: 'http://localhost:3001',
    filename: '[name].[hash].bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin(paths.dist, cleanOptions),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: 'index.html',
    })
  ],
  devServer: {
    contentBase: paths.dist,
    port: 3001,
    hot: true,
    historyApiFallback: true,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        secure: false
      }
    }
  }
})
