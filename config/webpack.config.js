/*eslint-disable no-var, one-var, func-names, indent, prefer-arrow-callback, object-shorthand, no-console, newline-per-chained-call, one-var-declaration-per-line, prefer-template, vars-on-top */
const path = require('path');
const fs = require('fs');
const paths = require('./paths');
const autoprefixer = require('autoprefixer');
const ExtractText = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';

// the clean options to use
let cleanOptions = {
  root:     paths.app,
  verbose:  true,
  dry:      false,
  allowExternal: true
}

const cssLoaders = [
  { loader: 'style' },
  {
    loader: 'css',
    options: {
      sourceMap: true,
    },
  },
  {
    loader: 'postcss',
    options: {
      sourceMap: true,
      plugins: [
        autoprefixer({
          browsers: ['last 2 versions'],
        }),
      ],
    },
  },
  {
    loader: 'sass',
    options: {
      sourceMap: true,
      outputStyle: 'compact',
    },
  },
];

module.exports = {
  context: paths.app,
  resolve: {
    alias: {
      'core-components': paths.coreComponents,
      'core-reducers': paths.coreReducers,
      'core-epics': paths.coreEpics,
      assets: paths.assets,
      modernizr$: paths.modernizrrc,
      test: paths.test,
    },
    modules: [paths.app, paths.nodeModules],
    extensions: ['*', '.js', '.jsx']
  },
  resolveLoader: {
    moduleExtensions: ['-loader'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader?cacheDirectory=true',
        }
      },
      {
        test: /\.scss$/,
        loader: isProd ? ExtractText.extract({
          use: cssLoaders.slice(1),
        }) : cssLoaders,
      },
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['url?limit=10000&minetype=application/font-woff&name=fonts/[name].[ext]'],
        include: /fonts/,
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['file?name=fonts/[name].[ext]'],
        include: /fonts/,
      },
    ]
  }
};
