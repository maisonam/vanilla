import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { routerReducer, routerMiddleware } from 'react-router-redux';

import history from 'modules/history';
import rootEpic from 'core-epics';
import rootReducer from 'core-reducers';

const epicMiddleware = createEpicMiddleware(rootEpic);

const reducer = combineReducers({
  ...rootReducer,
  router: routerReducer,
});


const middleware = [
  epicMiddleware,
  routerMiddleware(history),
];

/* istanbul ignore next */
if (process.env.NODE_ENV === 'development') {
  const { createLogger } = require('redux-logger');

  middleware.push(createLogger({ collapsed: true }));
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/* istanbul ignore next */

const configStore = (initialState = {}) => {
  const createStoreWithMiddleware = composeEnhancers(applyMiddleware(...middleware))(createStore);

  const store = createStoreWithMiddleware(reducer, initialState);

  if (module.hot) {
    module.hot.accept('core-reducers', () => {
      store.replaceReducer(require('core-reducers').default);
    });

    module.hot.accept('core-epics', () => {
      epicMiddleware.replaceEpic(require('core-epics').default);
    });
  }

  return {
    store,
  };
};

const { store } = configStore();

export { store };
