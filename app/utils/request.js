import { Observable } from 'rxjs/Observable';

export function ajaxGet(path) {
  return Observable.ajax.getJSON(path);
}
