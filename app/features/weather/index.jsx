import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Card from 'features/core/components/Card.jsx';

import { fetchWeather } from './actions'


export class Weather extends React.Component {

  static propTypes = {
    weather: PropTypes.object,
    onfetchWeather: PropTypes.func
  };

  componentDidMount() {
    const { onfetchWeather, weather: { loaded } } = this.props;
    if(!loaded){
      onfetchWeather();
    }
  }

  render() {
    const { weather: { data } } = this.props;

    return (
      <div key="Weather" className="app__home app__route">
        <Card className="app__card" content={this.buildWeatherCard(data)} />
      </div>
    );
  }

  buildWeatherCard(data) {
    const { description, where, temperature, unit } = data;
    return (
      <div className="app__card__weather">
        <h2>{where}</h2>
        <p>
          <span className="temp deg">
            {Math.round(temperature).toString()}
          </span>
          {unit}
        </p>
        <p>{description}</p>
      </div>
    );
  }
}

export default connect(
  state => ({ weather: state.openWeather }),
  {
    onfetchWeather: fetchWeather,
  }
)(Weather);
