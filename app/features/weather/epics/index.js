import { ajaxGet } from 'utils/request';
import { errorHandler } from 'core-epics/errorHandler';
import { ACTIONS, fetchWeatherSuccess } from '../actions';

function fetchWeatherRequest() {
  return ajaxGet(`api/weather0.json`)
    .map(fetchWeatherSuccess)
    .catch(errorHandler(ACTIONS.FETCH_FAILURE));
}

export function fetchWeather(action$) {
  return action$.ofType(ACTIONS.FETCH_REQUEST)
    .switchMap(fetchWeatherRequest);
}
