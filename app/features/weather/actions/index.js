import { action } from 'utils/actions';

export const ACTIONS = {
  FETCH_REQUEST: 'WEATHER_FETCH_REQUEST',
  FETCH_SUCCESS: 'WEATHER_FETCH_SUCCESS',
  FETCH_FAILURE: 'WEATHER_FETCH_FAILURE',
  FETCH_CANCEL: 'WEATHER_CANCEL',
};

export const fetchWeather = () => action(ACTIONS.FETCH_REQUEST);
export const fetchWeatherSuccess = (response) => action(ACTIONS.FETCH_SUCCESS, response);
