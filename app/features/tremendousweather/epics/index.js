import { ajaxGet } from 'utils/request';
import { errorHandler } from 'core-epics/errorHandler';
import { ACTIONS, fetchTremendousWeatherSuccess } from '../actions';

function fetchTremendousWeatherRequest() {
  return ajaxGet(`api/weather1.json`)
    .map(fetchTremendousWeatherSuccess)
    .catch(errorHandler(ACTIONS.FETCH_FAILURE));
}

export function fetchTremendousWeather(action$) {
  return action$.ofType(ACTIONS.FETCH_REQUEST)
    .switchMap(fetchTremendousWeatherRequest);
}
