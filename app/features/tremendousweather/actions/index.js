import { action } from 'utils/actions';

export const ACTIONS = {
  FETCH_REQUEST: 'TREMENDOUSWEATHER_FETCH_REQUEST',
  FETCH_SUCCESS: 'TREMENDOUSWEATHER_FETCH_SUCCESS',
  FETCH_FAILURE: 'TREMENDOUSWEATHER_FETCH_FAILURE',
  FETCH_CANCEL: 'TREMENDOUSWEATHER_CANCEL',
};

export const fetchTremendousWeather = () => action(ACTIONS.FETCH_REQUEST);
export const fetchTremendousWeatherSuccess = (response) => action(ACTIONS.FETCH_SUCCESS, response);
