import React from 'react';
import { connect } from 'react-redux';

import Card from 'features/core/components/Card.jsx';

import { fetchTremendousWeather } from './actions'

export class TremendousWeather extends React.Component {

  componentDidMount() {
    const {
      onfetchTremendousWeather,
      tremendousWeather: { loaded } } = this.props;
    if(!loaded){
      onfetchTremendousWeather();
    }
  }

  render() {
    const { tremendousWeather: { data, loaded } } = this.props;

    return (
      <div className="app__home app__route">
        {
          loaded &&
          <Card className="app__card" content={this.buildWeatherCard(data)} />
        }
      </div>
    );
  }

  buildWeatherCard(data) {
    const { unit, value } = data.main.temp;
    const { description } = data.weather[0];
    return (
      <div className="app__card__weather">
        <h2>{data.name}</h2>
        <p>
          <span className="temp">{Math.round(value).toString()}</span> {unit}
        </p>
        <p>{description}</p>
      </div>
    );
  }
}

export default connect(
  state => ({ tremendousWeather: state.tremendousWeather }),
  {
    onfetchTremendousWeather: fetchTremendousWeather,
  }
)(TremendousWeather);
