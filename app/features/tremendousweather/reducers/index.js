import { createReducer } from 'utils/helpers';
import { ACTIONS } from '../actions';

export const initialState = {
  data: {},
  fetching: false,
  loaded: false
};

export default {
  tremendousWeather: createReducer(initialState, {
    [ACTIONS.FETCH_REQUEST](state) {
      return {
        ...state,
        ...initialState,
        fetching: true,
        loaded: false
      };
    },
    [ACTIONS.FETCH_FAILURE](state, action) {
      return {
        ...state,
        error: action.payload,
        ...initialState,
        fetching: false
      };
    },
    [ACTIONS.FETCH_SUCCESS](state, action) {
      return {
        ...state,
        ...initialState,
        data: action.payload,
        fetching: false,
        loaded: true,
      };
    },
  })
};
