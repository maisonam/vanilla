/**
 * @module Reducers/Root
 * @desc Root Reducers
 */

import app from 'features/core/reducers/app';
import weather from 'features/weather/reducers';
import tremendousWeather from 'features/tremendousweather/reducers';


export default {
  ...app,
  ...weather,
  ...tremendousWeather,
};
