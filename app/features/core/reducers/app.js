import { createReducer } from 'utils/helpers';
import { ACTIONS } from '../actions';

export const initialState = {
  menuOpen: false
};

export default {
  app: createReducer(initialState, {
    [ACTIONS.TOGGLE_MENU](state) {
      return { ...state, ...initialState, menuOpen: !state.menuOpen };
    }
  })
};
