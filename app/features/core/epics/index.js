/**
 * @module Epics/Root
 * @desc Root Epics
 */

import { combineEpics } from 'redux-observable';
import { fetchWeather } from 'features/weather/epics';
import { fetchTremendousWeather } from 'features/tremendousweather/epics';

export default combineEpics(
  fetchWeather,
  fetchTremendousWeather,
);
