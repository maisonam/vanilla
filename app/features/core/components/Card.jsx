import React from 'react';

const Card = ({ children, content }) => {
  return (
    <div className="app__card">
      {content}
    </div>
  );
};


export default Card;
