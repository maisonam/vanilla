import React from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

export const Menu = ({ open, toggle}) => {
  return (
    <header className="app__header">
      <div className="app__container">
        <div className="app__header__menu">
          <button
            id="menu-btn"
            className={classNames({ 'menu-btn-open': open })}
            onClick={() => toggle()}
          >
            <span>Menu</span>
          </button>
        </div>
        <nav className="flex-full_width">
          <ul className={classNames('app__header__drawer', {'active': open })}>
            <li>
              <NavLink
                to="/"
                className="app__header__link"
                activeClassName="is-active"
                exact
              >
                 - Open weather
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/tremendous"
                className="app__header__link"
                activeClassName="is-active"
              >
                - Tremendous weather
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};
