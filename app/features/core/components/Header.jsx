import React from 'react';
import { connect } from 'react-redux';

import { Menu } from 'core-components/Menu';

import { toggleMenu } from '../actions'

export class Header extends React.Component {

  menuToggle = () => {
    const { onToggleMenu } = this.props;
    onToggleMenu();
  }

  render() {
    const { menuOpen } = this.props;
    return <Menu open={menuOpen} toggle={this.menuToggle}/>;
  }


}
export default connect(
  state => ({ menuOpen: state.app.menuOpen }),
  {
    onToggleMenu: toggleMenu,
  }
)(Header);
