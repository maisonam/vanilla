import { action } from 'utils/actions';

export const ACTIONS = {
  TOGGLE_MENU: 'APP_TOGGLE_MENU',
};

export const toggleMenu = () => action(ACTIONS.TOGGLE_MENU);
