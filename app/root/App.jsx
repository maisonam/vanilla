import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import history from 'modules/history';

import Header from 'core-components/Header';
import Weather from 'features/weather';
import TremendousWeather from 'features/tremendousweather';

export class App extends React.Component {

  render() {
    return (
      <ConnectedRouter history={history}>
        <div className="app">
          <Header/>
          <main className="app__main">
            <Switch>
              <Route exact path="/" component={Weather} />
              <Route exact path="/tremendous" component={TremendousWeather} />
            </Switch>
          </main>
        </div>
      </ConnectedRouter>
    );
  }
}

export default connect(
  state => ({ app: state.app }),
  {}
)(App);
