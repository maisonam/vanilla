// Rx
import 'vendor/rxjs';

import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import App from 'root/App';
import { AppContainer } from 'react-hot-loader';

import { store } from 'store';

import 'styles/main.scss';

export function renderApp(RootComponent) {
  const target = document.getElementById('react');

  /* istanbul ignore next */
  if (target) {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <RootComponent />
        </Provider>
      </AppContainer>,
      target
    );
  }
}

renderApp(App);

/* istanbul ignore next  */
if (module.hot) {
  module.hot.accept(
    'root/App',
    () => renderApp(require('root/App'))
  );
}
